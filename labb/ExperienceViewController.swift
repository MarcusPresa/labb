//
//  ExperienceViewController.swift
//  labb
//
//  Created by Marcus Presa Käld on 2018-11-27.
//  Copyright © 2018 Marcus Presa Käld. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    var sections: [[experienceObj]] = [
        [
            experienceObj(imageName: "microscope", title: "Elgiganten", year: "2016-Present", content: "Warehouse employee at elgiganten, help customer with warranty problems.")
        ],
        [
            experienceObj(imageName: "globe", title: "Jönköping University", year: "2017-Present", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        ]
    ]
    /*
    var cellContent: [experienceObj] = [
        experienceObj(imageName: "microscope", title: "Elgiganten", year: "2016-Present", content: "Warehouse employee at elgiganten, help customer with warranty problems."),
        experienceObj(imageName: "globe", title: "Jönköping University", year: "2017-Present", content: "Studying software engineering at Jönköping university")
    ]
 */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Experience"
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController {
            if let indexPath = sender as? IndexPath {
                let details = sections[indexPath.section][indexPath.row]
                destination.details = details
            }
        }
    }
    
}

extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let experiences = sections[indexPath.section][indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for :indexPath) as! ExperienceViewCell
        cell.setExperience(experience: experiences)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Work"
        }
        return "Education"
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailSegue", sender: indexPath)
    }
    
    
    
    
    
    
    
    
    
    
}
