//
//  experience.swift
//  labb
//
//  Created by Marcus Presa Käld on 2018-11-27.
//  Copyright © 2018 Marcus Presa Käld. All rights reserved.
//

import Foundation
struct experienceObj {
    var imageName: String
    var title: String
    var year: String
    var content: String
}
