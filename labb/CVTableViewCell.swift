//
//  CVTableViewCell.swift
//  labb
//
//  Created by Marcus Presa Käld on 2018-11-24.
//  Copyright © 2018 Marcus Presa Käld. All rights reserved.
//

import UIKit

class CVTableViewCell: UITableViewCell {

    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var cellPicture: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
