//
//  ExperienceDetailViewController.swift
//  table
//
//  Created by Marcus Presa Käld on 2018-11-25.
//  Copyright © 2018 Marcus Presa Käld. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var detailContent: UILabel!
    @IBOutlet weak var detailYear: UILabel!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var detailImage: UIImageView!
    var details: experienceObj = experienceObj(imageName: "", title: "", year: "", content: "")

    override func viewDidLoad() {
        super.viewDidLoad()
        detailImage.image = UIImage(named: details.imageName)
        detailTitle.text = details.title
        detailYear.text = details.year
        detailContent.text = details.content

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
