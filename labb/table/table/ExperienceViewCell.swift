//
//  ExperienceViewCell.swift
//  table
//
//  Created by Marcus Presa Käld on 2018-11-25.
//  Copyright © 2018 Marcus Presa Käld. All rights reserved.
//

import UIKit

class ExperienceViewCell: UITableViewCell {


    @IBOutlet weak var experienceYear: UILabel!
    @IBOutlet weak var experienceImageView: UIImageView!
    @IBOutlet weak var experienceTitle: UILabel!
    
    func setExperience(experience: experienceObj){
        experienceImageView.image = UIImage(named: experience.imageName)
        experienceTitle.text = experience.title
        experienceYear.text = experience.year
    }
}
