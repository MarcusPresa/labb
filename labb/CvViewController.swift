//
//  CvViewController.swift
//  labb
//
//  Created by Marcus Presa Käld on 2018-11-22.
//  Copyright © 2018 Marcus Presa Käld. All rights reserved.
//

import UIKit

class CvViewController: UIViewController {
    @IBOutlet weak var cvImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "CV"
        cvImage.layer.borderWidth = 1
        cvImage.layer.masksToBounds = false
        cvImage.layer.borderColor = UIColor.black.cgColor
        cvImage.layer.cornerRadius = cvImage.frame.height/2
        cvImage.clipsToBounds = true
        var yourImage: UIImage = UIImage(named: "marcus")!
        cvImage.image = yourImage
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
