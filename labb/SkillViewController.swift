//
//  SkillViewController.swift
//  labb
//
//  Created by Marcus Presa Käld on 2018-11-26.
//  Copyright © 2018 Marcus Presa Käld. All rights reserved.
//

import UIKit

class SkillViewController: UIViewController {

    @IBOutlet weak var uiViewAnimation: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Skills"
        uiViewAnimation.layer.cornerRadius = uiViewAnimation.frame.width/4
        uiViewAnimation.layer.masksToBounds = true;
        UIView.animate(withDuration: 2, delay: 0, options: [.autoreverse, .repeat], animations:{
            self.uiViewAnimation.transform = CGAffineTransform(scaleX: 7, y: 7)
        }, completion: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */





