//
//  ExperienceViewController.swift
//  table
//
//  Created by Marcus Presa Käld on 2018-11-25.
//  Copyright © 2018 Marcus Presa Käld. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {

    var cellContent: [experienceObj] = [
        experienceObj(imageName: "microscope", title: "Elgiganten", year: "2016-Present", content: "Warehouse employee at elgiganten, help customer with warranty problems."),
        experienceObj(imageName: "globe", title: "Jönköping University", year: "2017-Present", content: "Studying software engineering at Jönköping university")
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController {
            if let indexPath = sender as? IndexPath {
                let details = cellContent[indexPath.row]
                destination.details = details
            }
        }
    }

}

extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let experiences = cellContent[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for :indexPath) as! ExperienceViewCell
        cell.setExperience(experience: experiences)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Education"
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailSegue", sender: indexPath)
    }

    
    
    
    
    
    
    
    
    
}
